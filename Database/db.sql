CREATE TABLE if NOT EXISTS users(
	id INT UNSIGNED AUTO_INCREMENT NOT NULL  PRIMARY KEY,
	fullName VARCHAR(50) DEFAULT 'NOT NAME' NOT NULL,
	email VARCHAR(50) DEFAULT 'NOT EMAIL' NOT NULL UNIQUE,
	photo  VARCHAR(255) DEFAULT 'profile.png' NOT NULL,
	_password VARCHAR(255) NOT NULL,
	_status TINYINT DEFAULT 1,
	begDate DATETIME NOT NULL DEFAULT NOW(),
	endDate DATETIME
)ENGINE = INNODB CHARSET = UTF8;

CREATE TABLE if NOT EXISTS messages(
	id INT UNSIGNED AUTO_INCREMENT NOT NULL  PRIMARY KEY,
	_sendId INT UNSIGNED  NOT NULL,
	_acceptId INT UNSIGNED  NOT NULL,
	_type TINYINT DEFAULT 1,
	message VARCHAR(255) NOT NULL,
	_status TINYINT DEFAULT 1,
	begDate DATETIME NOT NULL DEFAULT NOW(),
	endDate DATETIME,
	FOREIGN KEY key_user_send(_sendId) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY key_user_accept(_acceptId) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE = INNODB CHARSET = UTF8;
