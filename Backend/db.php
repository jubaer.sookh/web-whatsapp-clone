<?php


    class DB{
        
            private $host;
            private $user;
            private $pass;
            private $dbname;
            private $char;
        
            protected $pdo;
        
        
        function __construct($host="localhost",$user="root",$pass="",$dbname="corona",$char="utf8"){
            
            $this->host=$host;
            $this->user=$user;
            $this->pass=$pass;
            $this->dbname=$dbname;
            $this->char=$char;
            
            
            try{
                
                $this->pdo=new PDO("mysql:host={$this->host};dbname={$this->dbname};charset={$this->char}",$this->user,$this->pass);
                
            }
            catch(PDOException $err){
                
                echo $err->getMessage();
                
            }
            
        }
        
        function __destruct(){
            $this->pdo=null;
        }
        
        public function Insert($table,$args){
            
            $ques="";
            $row="";
            $arg=[];
                
            foreach($args as $k=>$v){
                
                $row.=",$k";//,login,pass,fullname
                $ques.=",?";//,?,?,?
                
                array_push($arg,$v);//["ali","1234","Aliyev Ali"]
                
            }
                $row=substr($row,1);//login,pass,fullname
                $ques=substr($ques,1);//?,?,?
             
            
            $q="INSERT INTO $table ($row) VALUES ($ques)";
            
            $this->pdo->prepare($q)->execute($arg);
            
            
            
            return $this;
            
        }
        
        public function lastInsertId(){
            
            return  intval($this->pdo->lastInsertId());
        }
        
        public function Select($table,$rows=["*"],$whe=true,$QUERY='',$res="name"){
               
            $row="";
            
            foreach($rows as $r){
                
                $row.=",$r";
            }
            
            $row=substr($row,1);
            
            $q="SELECT $row FROM $table WHERE $whe $QUERY";
       
            
            $query=$this->pdo->query($q);
            $query->execute();

            switch($res){
                    
                case 'name':{
                    
                    $data=$query->fetch(PDO::FETCH_ASSOC);
                    break;
                }
                    
                default:{
                    $data=$query->fetch();
                }
            }
           
            return $data;
            
        }
        
        public function SelectAll($table,$rows=["*"],$whe=true,$QUERY='',$res="name"){
               
            $row="";
            
            foreach($rows as $r){
                
                $row.=",$r";
            }
            
            $row=substr($row,1);
            
            $q="SELECT $row FROM $table WHERE $whe $QUERY";
            
            $query=$this->pdo->query($q);
            $query->execute();
            
            $count=$query->rowCount();

            switch($res){
                    
                case 'name':{
                    
                    $data=$query->fetchAll(PDO::FETCH_ASSOC);
                    break;
                }
                    
                default:{
                    $data=$query->fetchAll();
                }
            }
           
            
            return [$count,$data];
            
        }
        
        public function Update($table,$args,$whe){
            
            $set="";//,?,?,?
            $arg=[];
            
            foreach($args as $k=>$v){
                
                $set.=",$k=?";//,login=?,pass=?,fullname=?
                
                array_push($arg,$v);//["ali","1234","Aliyev Ali"]
            }
            
            $set=substr($set,1);//login=?,pass=?,fullname=?
            
            
            $q="UPDATE $table SET $set WHERE $whe";
            
            $query=$this->pdo->prepare($q);
            $query->execute($arg);
            
            return $query->rowCount();
            
        }
        
        public function Delete($table,$whe=true){
            
            $q="DELETE FROM $table WHERE $whe";
            
            $query=$this->pdo->exec($q);
            
            return $query;
            
        }
        
        public function Transaction(){
            
            $this->pdo->beginTransaction();
            
            return $this;
            
        }
        
        public function Commit(){
            
            $this->pdo->commit();
            
            return $this;
        }
        
        public function RollBack(){
            
            $this->pdo->rollBack();
            
            return $this;
        }
                
        
    }

    $db=new DB("localhost","u160423359_aticiliq","86^!y2TzW5IJ","u160423359_aticiliq");

/*
    $db->Insert("users",["login"=>"Ali","pass"=>"1234"])->Insert("users",["login"=>"Aqil","pass"=>"12345"])->Insert("users",["login"=>"Arif","pass"=>"123456"]);
*/

//$db->Select("users",["login","pass"],["id","_status"],[">","="],[1,true]);
//print_r($db->Select("users",['*'],"login='Aqil'"));

/*
$d=$db->Select("users",["login","pass"],"id>3 and id<5");
$d=$db->Select("users",["login"],"id>5","name");
$d=$db->SelectAll("users",["*"],"id>1","LIMIT 2","name");
*/

/*
$d=$db->Update("users",["fullname"=>"Ceferov Aqil"],"id=5");
*/

/*
$d=$db->Delete("users");//her birini siler
$d=$db->Delete("users","id=34");//id=34 olani iler
*/

/*
    $d=$db->Transaction()->Update("users",["fullname"=>"Aliyev Ali"],"id=10");

    if($d==true){
        $db->Commit();
    }else{
       $db->RollBack(); 
    }
*/



?>
