<?php

    include_once "header.php";
    include_once "functions.php";
    include_once "db.php";


    if($_SERVER["REQUEST_METHOD"]=="GET"){


        if(isset($_GET['_sendId']) && isset($_GET['_acceptId']) && isset($_GET['pag'])){
            
            $pag=intval($_GET['pag'])*10;
            
            $_sendId=$_GET['_sendId'];
            $_acceptId=$_GET['_acceptId'];

            if($pag<0) $res=$db->SelectAll("messages",["*"],"_sendId={$_sendId} AND _acceptId={$_acceptId} OR _sendId={$_acceptId} AND _acceptId={$_sendId} AND _status!=0");

            else $res=$db->SelectAll("messages",["*"],"_sendId={$_sendId} AND _acceptId={$_acceptId} OR _sendId={$_acceptId} AND _acceptId={$_sendId} AND _status!=0","LIMIT {$pag}, 10");

            response($res);

        }else{
                    
            response("INFORMASIYALAR GONDERILMEYIB");
        }

    }else{

        response("ERROR 404");
    }


?>
