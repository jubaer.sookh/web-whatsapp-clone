<?php

    include_once "header.php";
    include_once "functions.php";
    include_once "db.php";

    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $POST=file_get_contents("php://input");
        $POST=json_decode($POST,true);

        if(isset($POST['_sendId']) && isset($POST['_type']) && isset($POST['message']) && isset($POST['_acceptId'])){

            $_sendId=$POST['_sendId'];
            $_acceptId=$POST['_acceptId'];
            $message=$POST['message'];
            $_type=$POST['_type'];

            $res=$db->Insert("messages",["_sendId"=>$_sendId,"_type"=>$_type,"message"=>$message,"_acceptId"=>$_acceptId])->lastInsertId();

            response($res);

        }else{
                
            response("INFORMASIYALAR GONDERILMEYIB");
        }

    }else{

        response("ERROR 404");
    }


?>